> ### **Bug ID** 02-web-elixeum
> [Image](/bug-reports/02_bug-report_web-elixeum/02_bug-report_web-elixeum.PNG) to this report.

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|Functionality| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Occasionally|Exploratory test|

### **Issue description**
Dev tool logs service worker navigation preload request was cancelled before "preloadResponse" settled.

* **Expected result**
<br>There is no log during browsing.

* **Actual result**
<br>There is actual log during browsing.

* **Error Messages**
```js
The service worker navigation preload request was cancelled before 'preloadResponse' settled. If you intend to use 'preloadResponse', use waitUntil() or respondWith() to wait for the promise to settle.
```

```js
14The FetchEvent for "<URL>" resulted in a network error response: the promise was rejected.
```

### **Reproduction**
1. Open the website.
1. Open the Dev tools. 
1. Choose the english version of the web.
1. Click on other page in menu.
1. Change to another webpage until the error occurs.
