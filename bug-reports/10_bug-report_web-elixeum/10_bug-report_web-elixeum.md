> ### **Bug ID** 09-web-elixeum
|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|Functionality| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
It´s possible to leave a telephone number as string.

* **Expected result**
The form won´t submit a text as a telephone number.

* **Actual result**
The form accepted non-validated telephone number and show the message of the success. 

### **Reproduction**
1. Go to website.
1. Scroll down to the footer of the page.
1. Click on "Zanechat číslo".
1. Fill the input with the string "number".
1. Submit the form.