> ### **Bug ID** 09-web-elixeum 
|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|UI| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
The english text content is missing in Portal´s, Project management´s and Construction site diary´s sections.

* **Expected result**
The full content in english on pages Portal, Project management and Construction site diary.

* **Actual result**
The part of the text is missing on pages Portal, Project management and Construction site diary.

### **Reproduction**
1. Go to website.
1. Change to english version.
1. Go to webpage Products.
1. Scroll to the section Portal.
1. Click on Learn more.
1. Scroll down.
1. Get back to the Products page.
1. Scroll down to Project management
1. Click on Learn more.
1. Get back to the Products page.
1. Scroll to the Construction site diary.
1. Click on Learn more.