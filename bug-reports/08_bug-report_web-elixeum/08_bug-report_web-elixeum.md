> ### **Bug ID** 08-web-elixeum
> [Image](/bug-reports/08_bug-report_web-elixeum/) for this report.

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|UI| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
The text is not aligned to left on the Products webpage.

* **Expected result**
The text is aligned to the left.

* **Actual result**
The text is not aligned to the left.

### **Reproduction**
1. Go to website.
1. Go to webpage Products.
1. Check the section Virtual Office.