> ### **Bug ID** 06-web-elixeum 
> There are images in the folder 06_bug-report_web-elixeum

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|UI| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
The inconsistence positioning of the icon "Leták".

* **Expected result**
The icon would be set at the same position in every section.

* **Actual result**
The icon is set differently than other section.

### **Reproduction**
1. Go to website.
1. Go to Produkty page in Czech version of the website.