> ### **Bug ID** 03-web-elixeum
> There are images in the folder 03_bug-report_web-elixeum

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|UI| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
The text on the Cookies bar is in white color as background color.

* **Expected result**
<br>The text is visible on the cookie bar.

* **Actual result**
<br>The text isn´t visible.

### **Reproduction**
1. Go to the webpage.
1. Clear the cookies of the webpage.
1. The cookie bar should be visible.
1. Go to Case studies or Contacts.