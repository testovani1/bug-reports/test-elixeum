> ### **Bug ID** 01-web-elixeum
> There are images for this report, please check the folder 01_bug-report_web-elixeum.

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|Functionality| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
The form does not validate the e-mail and the telephone number.

* **Expected result**
<br>The form warns about non-validated inputs during filling.

* **Actual result**
<br>The form closes and the warn sign about failed form submission appears on the right down corner of the browser.

### **Reproduction**
1. Open the form with the button Domluvte schůzku at the footer of the browser.
1. Fill the form input for the e-mail with string without the at-sign.
1. Fill the telephone input with string, not numbers.
1. Submit the form.
