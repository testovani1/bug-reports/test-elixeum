> ### **Bug ID** 05-web-elixeum
> There are images in the folder 05_bug-report_web-elixeum

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|UI| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|


### **Issue description**
When hovering with the cursor on the button with name "Technická data" in the Cookies bar, the cursor doesn´t change its shape from arrow to hand.

* **Expected result**
<br>The cursor would visually suggest that element is dynamic and could be open. 

* **Actual result**
<br>The cursor suggest that it´s possible to fill with text. 

* **UX**
<br> The arrow be position to the left, next to the headline "Technická data". It would help the user to realize that element is openable.

### **Reproduction**
1. Go to the website.
1. Clear the GDPR cookie consent.
1. Open the GDPR Cookie consent.
1. Go to "Nastavení".
1. Hover on one of the arrow icons on the right.