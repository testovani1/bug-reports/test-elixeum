> ### **Bug ID** 04-web-elixeum
> [Image](/bug-reports/04_bug-report_web-elixeum/04_bug_report_web_elixeum.PNG) for this bug

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|Language content| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|


### **Issue description**
The text on the GDPR Cookie consent is in Czech language in English version of the web.

* **Expected result**
<br>English version of the GDPR Cookie consent on English version of the page.

* **Actual result**
<br>Czech version of the GDPR Cookie consent on English version of the page.

### **Reproduction**
1. Go to the webpage.
1. Clean the cookies settings.
1. Change the version of the web to english.
1. Check the GDPR Cookie consent. 