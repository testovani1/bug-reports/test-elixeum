> ### **Bug ID** 07-web-elixeum 
> There are images in the folder 07_bug-report_web-elixeum

|Type| Priority| Severity| Environment| Frequency|Source|
|:---------:|:---------:|:---------:|:-----------|:----------:|:--------:|
|UI| Low| Low|Windows 10 - Chrome ver 107.5304.88 (64 bit)| Every time|Exploratory test|

### **Issue description**
The english page Products missing text content.

* **Expected result**
The text content is accessible.

* **Actual result**
The most of the text is missing

### **Reproduction**
1. Go to website.
1. Go to webpage Products.