# #3 Test case: Check aligned elements

## Type
Positive test case

## Summary
Check aligned elements that are positioned in the right order.

## Pre conditions
There is no pre-condition for the test case.

## Steps
1. Go to website.
1. Open dev tools.
1. Use the tool for inspecting elements.
1. Check elements if they are exactly aligned.

## Post conditions
There is no post-condition for the test case.

## Expected results
The elements are aligned.

## Actual results
- [ ] The menu on page is not aligned with element of language version and button Make an appointment.
- [ ] At the footer on the main page the section We will call you back and Send us an inquiry are not aligned next to each other.

## Status
Failed.