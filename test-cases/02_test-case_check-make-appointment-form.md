# #2 Test case: Check make an appointment form

## Type
Negative test case

## Summary
Check form Make an appointment.

## Pre conditions
Input for the e-mail is without at-sign. Input for telephone number is a string, not a number. Other input are not restricted to specific string, characters or types.

```js
{
  Name: "Jan",
  Surname: "Novak",
  TheCompany: "Firm",
  eMail: "nonsense",
  phoneNumber: "nonsense",
  message: "nonsense"
}
```

## Steps
1. Go to website.
1. Click on make an appoinment.
1. Fill the form and submit. 

## Post conditions
There is no post-condition for the test case.

## Expected results
The form doesn´t close. It visually warns immedietely which inputs receive the wrong value.

## Actual results
- [ ] The form is closed.
- [ ] There is a warn message in Czech language on the right down corner.
- [ ] When open the form, there is any sign which inputs receive wrong values.

## Status
Failed.