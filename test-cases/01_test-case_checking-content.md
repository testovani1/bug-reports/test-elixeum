# #1 Test case: Check text content for english version of Products page

## Type
Positive test case

## Summary
Check English version of the page for text content at the [Product](https://www.elixeum.com/products) webpage.

## Pre conditions
There is no pre-condition for the test case.

## Steps
1. Go to website of english version of Products page.
1. Check section Virtual Office.
1. Check if the section have all text and icons amd the image attachments.
1. Click on Learn more.
1. Check icons, text and image attachments.
1. Check other sections: Contracts, Portal, Project management, Constructions site diary, Attendace, Administration and Reporting.

## Post conditions
There is no post-condition for the test case.

## Expected results
All the sections of the Products page have english version of the image attachements, text content and icons.

## Actual results
- [ ] Image attachments are in Czech language.
- [ ] Section Portal missing text content.
- [ ] Section Project management missing text content.
- [ ] Sectopm Construction site diary missing text content.
- [ ] The learn more page for Portal missing text content.
- [ ] The learn more page for Project management missing text content.
- [ ] The learn more page for Construction site Diary missing text content.

## Status
Failed.